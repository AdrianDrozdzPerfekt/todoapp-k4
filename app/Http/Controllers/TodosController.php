<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Todo;

class TodosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //pobranie notatek
            //WARIANT 1.
            //$todos = Todo::all();
        //WARIANT 2. z sortowaniem
        $todos = Todo::orderBy('created_at', 'desc')->get();
        //wyswietlenie widoku
        //oraz przekazanie listy notatek do zmiennej
        return view('todos.index')
            ->with('notatki', $todos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('todos.create');   
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'due' => 'required',
            'description' => 'required|min:15',
        ]);

        $todo = new Todo();
        $todo->title = $request->input('title');
        $todo->due = $request->input('due');
        $todo->description = $request->input('description');

        try {
            $todo->save();
            return redirect('/todos')
                        ->with('success', 'Dodano pomyślnie');
        } catch (\Exception $e) {
            return redirect('/todos')
                        ->with('error', 'Nie dodano. ' . $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        /*
        try {

        } catch (\Execption $e) {

        }
        */
        try {
            //znalezienie notatki
            $todo = Todo::findOrFail($id);
            //zwrocenie notaki do widoku
            //->with('todo', $todo) -> przekaz zawartosc zmiennej
            // $todo do widoku jako zmienna $todo
            return view('todos.show')->with('todo', $todo);
        } catch (ModelNotFoundException $e) {
            return redirect('/todos')->with('error','Nie znaleziono notatki');
        } catch (\Execption $e) {
            return redirect('/')->with('error','Wystąpił nieoczekiwany błąd');
        }
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            //znalezienie notatki
            $todo = Todo::findOrFail($id);
            //zwrocenie notaki do widoku
            //->with('todo', $todo) -> przekaz zawartosc zmiennej
            // $todo do widoku jako zmienna $todo
            return view('todos.edit')->with('todo', $todo);
        } catch (ModelNotFoundException $e) {
            return redirect('/todos')->with('error','Nie znaleziono notatki');
        } catch (\Execption $e) {
            return redirect('/')->with('error','Wystąpił nieoczekiwany błąd');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'due' => 'required',
            'description' => 'required|min:15',
        ]);

        $todo = Todo::findOrFail($id);
        $todo->title = $request->input('title');
        $todo->due = $request->input('due');
        $todo->description = $request->input('description');

        //$todo->isDirty(); //zwraca `true` jesli ma niezapisane zmiany
        //jesli nie ma zmian
        if (!$todo->isDirty()) {
            return redirect()->route('todos.edit', ['id' => $todo->id])
                        ->with('warning', 'Brak zmian');
        }

        try {
            $todo->save();
            return redirect('/todos')
                        ->with('success', 'Zapisano pomyślnie');
        } catch (\Exception $e) {
            return redirect('/todos')
                        ->with('error', 'Nie zapisano. ' . $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            //znalezienie notatki
            $todo = Todo::findOrFail($id);
            
            //jesli pomyslnie usunieto notatke
            if ($todo->delete()) {
                //przekieruj na liste notatek z sukcesem
                return redirect('/todos')->with('success','Usunięto notatkę');
            } else { //jesli nie usunieto notatki
                return redirect('/todos')->with('error','Nie usunięto notatki');
            }

        } catch (ModelNotFoundException $e) {
            return redirect('/todos')->with('error','Nie znaleziono notatki');
        } catch (\Execption $e) {
            return redirect('/')->with('error','Wystąpił nieoczekiwany błąd');
        }
    }
}
