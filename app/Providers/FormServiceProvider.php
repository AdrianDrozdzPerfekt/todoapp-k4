<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Form;

class FormServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        #Form::component('NAZWA_KOMPONENTU', 'ADRES_PLIKU_WIDOKU/SZABLONU', ['TABLICA','ZMIENNYCH'])
        Form::component('bsText', 'components.form.text', ['name', 'value', 'label', 'attributes']);
        Form::component('bsTextarea', 'components.form.textarea', ['name', 'value', 'label', 'attributes']);
        Form::component('bsSubmit', 'components.form.submit', ['label', 'attributes']);
        Form::component('bsCancel', 'components.form.cancel', ['url', 'label', 'attributes']);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
