<div class="form-group">
    {{ Form::submit($label, array_merge(['class' => 'btn btn-primary pull-right'], $attributes)) }}
</div>