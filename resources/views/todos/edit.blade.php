@extends('layouts.app')

@section('content')
	<div class="row">
		<div class="col-xs-12 text-success">
			<h1>Edycja TODO</h1>
		</div>
		<div class="col-xs-12 col-md-8 col-md-offset-2">
			{!! Form::open(['action' => ['TodosController@update', $todo->id], 'method' => 'POST']) !!}
			    {{Form::bsText('title', $todo->title, 'Tytuł notatki', [])}}
			    {{Form::bsText('due', $todo->due, 'Data wymagalnośc', [])}}
			    {{Form::bsTextarea('description', $todo->description, 'Opis', ['rows' => '10'])}}
			    {{Form::hidden('_method','PUT')}}
			    {{Form::bsCancel('/todos', 'Anuluj')}}
			    {{Form::bsSubmit('Zapisz', ['class' => 'btn btn-success pull-right'])}}
			{!! Form::close() !!}
		</div>
	</div>
@endsection