@extends('layouts.app')

@section('content')
	@if(count($notatki))
		@foreach($notatki as $notatka)
		<div class="row">
			<div class="col-xs-12">
				<div class="well well-lg todo-in-list">
					<h1>
						<a href="/todos/{{$notatka->id}}">{{$notatka->title}}</a>
						<span class="pull-right text-danger">
							{{$notatka->due}}
						</span>
					</h1>
				</div>
			</div>
		</div>
		@endforeach
	@endif
@endsection