@extends('layouts.app')

@section('content')
	<div class="row">
		<div class="col-xs-12">
			<h1>{{$todo->title}}</h1>
			<hr>
			<p>{{$todo->description}}</p>
			<p class="text-danger">{{$todo->due}}</p>
			<hr>
			<a href="/todos/{{$todo->id}}/edit" class="btn btn-primary pull-left">Edycja</a>
			{!! Form::open([
				'action' => ['TodosController@destroy', $todo->id], 
				'method' => 'POST', 
				'onsubmit' => 'return confirm("Czy chcesz usunąć notatkę?")'
			]) !!}
				{{Form::hidden('_method','DELETE')}}
			    {{Form::bsSubmit('Usuń', ['class' => 'btn btn-danger pull-right'])}}
			{!! Form::close() !!}
		</div>
	</div>
@endsection