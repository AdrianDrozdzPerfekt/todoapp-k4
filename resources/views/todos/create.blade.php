@extends('layouts.app')

@section('content')
	<div class="row">
		<div class="col-xs-12 text-success">
			<h1>Dodaj TODO</h1>
		</div>
		<div class="col-xs-12 col-md-8 col-md-offset-2">
			{!! Form::open(['action' => 'TodosController@store', 'method' => 'POST']) !!}
			    {{Form::bsText('title', null, 'Tytuł notatki', [])}}
			    {{Form::bsText('due', null, 'Data wymagalnośc', [])}}
			    {{Form::bsTextarea('description', null, 'Opis', ['rows' => '10'])}}
			    {{Form::bsSubmit('Dodaj', [])}}
			{!! Form::close() !!}
		</div>
	</div>
@endsection