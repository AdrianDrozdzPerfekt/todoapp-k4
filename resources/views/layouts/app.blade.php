<!DOCTYPE html>
<html lang="">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>TodoApp</title>
		<link rel="stylesheet" href="/css/app.css">
	</head>
	<body>
		@include('inc.navbar')
		<div class="container">
			@include('inc.alerts')
			@yield('content')
		</div>
		<script src="/js/app.js"></script>
	</body>
</html>